-- Création de la base de données
CREATE DATABASE IF NOT EXISTS mydb;
USE mydb;

-- Création de la table Concert
CREATE TABLE IF NOT EXISTS Concert (
    id INT AUTO_INCREMENT PRIMARY KEY,
    artiste VARCHAR(120) NOT NULL,
    nb_places INT NOT NULL,
    date_debut DATETIME NOT NULL,
    lieu TEXT NOT NULL,
    description TEXT
);

-- Création de la table Reservation
CREATE TABLE IF NOT EXISTS Reservation (
    id_concert INT NOT NULL,
    id_utilisateur INT NOT NULL,
    statut ENUM('a_confirme', 'confirme', 'annule') DEFAULT 'a_confirme',
    date_reservation DATETIME NOT NULL
);

-- Création de la table Utilisateur
CREATE TABLE IF NOT EXISTS Utilisateur (
    id INT AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(36) NOT NULL
);

-- Insertion des données pour les utilisateurs
INSERT INTO Utilisateur (pseudo)
VALUES ('Toto'), ('Vador'), ('Noob');

-- Insertion des données pour les concerts
INSERT INTO Concert (artiste, nb_places, date_debut, lieu, description)
VALUES ('Metallica', 10000, '2023-06-15 20:00:00', 'Stade Olympique', 'Les légendes du thrash metal, Metallica, enflamment la scène du Stade Olympique avec leur tournée WorldWired.'),
       ('Iron Maiden', 12000, '2023-07-25 21:00:00', 'Le Dôme', 'Iron Maiden revient avec leur tournée Legacy of the Beast, offrant un spectacle inoubliable pour les fans de heavy metal.'),
       ('Slipknot', 8000, '2023-09-10 19:30:00', 'Arènes Métalliques', 'Le groupe de metal alternatif Slipknot déchaîne un chaos sonore dans les Arènes Métalliques lors de leur tournée We Are Not Your Kind.');

-- Insertion des données pour les réservations
INSERT INTO Reservation (id_concert, id_utilisateur, statut, date_reservation)
VALUES (1, 1, 'a_confirme', '2023-05-05 12:30:00'),
       (2, 2, 'confirme', '2023-05-18 15:45:00'),
       (3, 3, 'annule', '2023-06-20 18:00:00');
