const express = require('express')
const router = express.Router()
const connection = require('../db')
const { mapConcertoResourceObject } = require('../hal')
const helpers = require('../helpers')

// Route pour obtenir les informations des concerts
router.get('/concerts', (req, res) => {
  const baseUrl = req.protocol + '://' + req.get('host')

  connection.query('SELECT * FROM Concert;',  (error, rows, results) => {
    if (error) throw error

    const concerts = rows.map(concert => mapConcertoResourceObject(concert, baseUrl))

    const response = {
      _links: {
        self: { href: `${baseUrl}/concerts` }
      },
      _embedded: {
        concerts: concerts
      }
    }
    res.setHeader('Content-Type', 'application/hal+json')
    res.send(JSON.stringify(response, null, 2))
  })
})


// Route pour obtenir les informations d'un concert spécifique
router.get('/concerts/:id', (req, res) => {
  const baseUrl = req.protocol + '://' + req.get('host')
  const id = req.params.id
  connection.query('SELECT * FROM Concert WHERE id = ?', [id], (error, results) => {
    if (error) throw error
    if (results.length === 0) {
      res.status(404).send('Concert non trouvé')
    } else {
      const concert = results[0]
      res.setHeader('Content-Type', 'application/hal+json')
      res.send(JSON.stringify({
        _links: {
          self: { href: `${baseUrl}/concerts/${concert.id}` },
          name: `${concert.artiste}`,
          reservations: { 
            href: `${baseUrl}/concerts/${concert.id}/reservations`,
            name: `Réservations`,
            templated: false 
           }
        },
        _embeded: {
          id: concert.id,
          artiste: concert.artiste,
          nb_places: concert.nb_places,
          date_debut: concert.date_debut,
          lieu: concert.lieu,
          description: concert.description
        }
      },null,2))
    }
  })
})


// Route pour obtenir les réservations pour un concert spécifique
router.get('/concerts/:id/reservations', (req, res) => {
  const baseUrl = req.protocol + '://' + req.get('host')
  const id = req.params.id
  connection.query('SELECT * FROM Reservation JOIN Utilisateur ON Reservation.id_utilisateur = Utilisateur.id WHERE Reservation.id_concert = ?', id, (error, results) => {
    if (error) throw error
    if (results.length === 0) {
      res.status(404).send('Aucune réservation pour ce concert')
    } else {
      const reservations = results.map(reservation => {
        return {
          id: reservation.id_utilisateur,
          name: reservation.pseudo,
          status: reservation.statut,
          date: reservation.date_reservation,
          _links: {
            self: { href: `${baseUrl}/concerts/${id}/reservations/${reservation.id_utilisateur}` },
            utilisateur: { href: `${baseUrl}/utilisateurs/${reservation.id_utilisateur}` }
          }
        }
      })
      res.setHeader('Content-Type', 'application/hal+json')
      res.send(JSON.stringify({
        _links: {
          self: { href: `${baseUrl}/concerts/${id}/reservations` },
          name: `Réservations pour le concert ${id}`,
        },
        _embedded: {
          reservations: reservations
        }
      }, null, 2))
    }
  })
})


// Route pour ajouter une réservation pour un concert spécifique
router.post('/concerts/:id/reservations', (req, res) => {
/* #swagger.parameters['pseudo'] = {
  in: 'formData',
  description: "Le pseudo de l'utilisateur qui effectue la réservation",
  required: true,
  type: 'string',
} */
  const id = req.params.id
  const { pseudo } = req.body
  const ticketCount = 1
  const dateReservation = new Date().toISOString().slice(0, 19).replace('T', ' ')

  // Vérifier si le concert existe
  connection.query('SELECT * FROM Concert WHERE id = ?', id, (error, results) => {
    if (error) throw error
    if (results.length === 0) {
      res.status(404).send('Concert non trouvé')
    } else {
      const concert = results[0]
      connection.query(
        "SELECT SUM( (SELECT nb_places FROM Concert WHERE id = ?) - (SELECT COUNT(*) FROM Reservation WHERE statut != 'annule' AND id_concert = ?) ) AS nb_places_dispo",
        [id,id],
        (error, results) => {
          if (error) throw error
          const remainingTickets = results[0].nb_places_dispo
          console.log(remainingTickets)
          if (remainingTickets < 0) {
            res.status(400).send('Nombre de places insuffisant')
          } else {
            // Vérifier si l'utilisateur existe déjà
            connection.query('SELECT * FROM Utilisateur WHERE pseudo = ?;', pseudo, (error, results) => {
              if (error) throw error
              let utilisateurId
              if (results.length === 0) {
                // Retourne une erreur si l'utilisateur s'il n'existe pas
                res.status(403).send('Veuillez créer votre compte avant de faire une réservation')
              } else {
                // Utilisateur existant
                utilisateurId = results[0].id
                // Vérifier si l'utilisateur a déjà une réservation pour ce concert
                connection.query('SELECT * FROM Reservation WHERE id_concert = ? AND id_utilisateur = ?', [id, utilisateurId], (error, results) => {
                  if (error) throw error
                  if (results.length > 0) {
                    // L'utilisateur a déjà une réservation pour ce concert
                    res.status(400).send(`${pseudo} a déjà une réservation pour ce concert`)
                  } else {
                    // Ajouter la réservation
                    helpers.insertReservation(id, utilisateurId, ticketCount, dateReservation, remainingTickets, res)
                  }
                })
              }
            })
          }
        }
      )
      // Vérifier si il reste suffisamment de places pour la réservation
      
    }
  })
})


// Route pour confirmer une réservation pour un concert spécifique
router.put('/concerts/:id/utilisateurs/:utilisateurId/confirmation', (req, res) => {
  // Récupérer l'ID du concert et l'ID de l'utilisateur depuis les paramètres de l'URL
  const id = req.params.id
  const utilisateurId = req.params.utilisateurId

  // Vérifier si le concert existe
  connection.query('SELECT * FROM Concert WHERE id = ?', id, (error, results) => {
    if (error) throw error
    if (results.length === 0) {
      res.status(404).send('Concert non trouvé')
    } else {
      // Vérifier si la réservation existe et est en statut "a_confirmer"
      connection.query('SELECT * FROM Reservation WHERE id_concert = ? AND id_utilisateur = ? AND statut = "a_confirme"', [id, utilisateurId], (error, results) => {
        if (error) throw error
        if (results.length === 0) {
          res.status(404).send('Réservation non trouvée ou déjà confirmée/annulée')
        } else {
          // Confirmer la réservation en changeant son statut en "confirme"
          connection.query('UPDATE Reservation SET statut = "confirme" WHERE id_concert = ? AND id_utilisateur = ?', [id, utilisateurId], (error, results) => {
            if (error) throw error
            // Envoyer un message de succès confirmant la confirmation de la réservation
            res.send(`Réservation pour l'utilisateur ${utilisateurId} confirmée pour le concert ${id}`)
          })
        }
      })
    }
  })
})



// Route pour annuler une réservation pour un concert spécifique
router.put('/concerts/:id/utilisateurs/:utilisateurId/annulation', (req, res) => {
  // Récupérer l'ID du concert et l'ID de l'utilisateur depuis les paramètres de l'URL
  const id = req.params.id
  const utilisateurId = req.params.utilisateurId

  // Vérifier si le concert existe
  connection.query('SELECT * FROM Concert WHERE id = ?', id, (error, results) => {
    if (error) throw error
    if (results.length === 0) {
      res.status(404).send('Concert non trouvé')
    } else {
      // Vérifier si la réservation existe et est en statut "a_confirmer"
      connection.query('SELECT * FROM Reservation WHERE id_concert = ? AND id_utilisateur = ? AND statut = "a_confirme"', [id, utilisateurId], (error, results) => {
        if (error) throw error
        if (results.length === 0) {
          res.status(404).send('Réservation non trouvée ou déjà confirmée/annulée')
        } else {
          // Annuler la réservation en changeant son statut en "annule"
          connection.query('UPDATE Reservation SET statut = "annule" WHERE id_concert = ? AND id_utilisateur = ?', [id, utilisateurId], (error, results) => {
            if (error) throw error
            // Envoyer un message de succès confirmant l'annulation de la réservation
            res.send(`Réservation pour l'utilisateur ${utilisateurId} annulée pour le concert ${id}`)
          })
        }
      })
    }
  })
})


// Route pour afficher toutes les réservations d'un concert spécifique pour l'administrateur
router.get('/admin/:adminId/concerts/:id/reservations', (req, res) => {
  // Récupérer l'ID de l'administrateur et l'ID du concert depuis les paramètres de l'URL
  const adminId = req.params.adminId;
  const id = req.params.id;

  // Vérifier si l'utilisateur est un administrateur en imaginant que son id est 1
  if (adminId === '1') {
    // Vérifier si le concert existe
    connection.query('SELECT * FROM Concert WHERE id = ?', id, (error, results) => {
      if (error) throw error;
      if (results.length === 0) {
        res.status(404).send('Concert non trouvé');
      } else {
        // Récupérer toutes les réservations pour ce concert
        connection.query('SELECT * FROM Reservation WHERE id_concert = ? AND statut = "confirme"', id, (error, results) => {
          if (error) throw error;
          res.send(results);
        });
      }
    });
  } else {
    // Si l'utilisateur n'est pas l'administrateur, envoyer un message d'erreur avec le statut 403
    res.status(403).send('Accès refusé: Vous devez être administrateur pour accéder à cette route');
  }
});


// Exporter le routeur pour l'utiliser dans d'autres fichiers
module.exports = router