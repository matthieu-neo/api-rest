
const concert = {
  id: 1,
  title: "Concert de rock",
  artist: "The Rolling Stones",
  date: "2022-06-15",
  venue: "Stade de France",
  tickets: {
    total: 1000,
    remaining: 500
  },
  reservations: [
    {
      id: 1,
      name: "John",
      email: "john@example.com",
      ticketCount: 2
    },
    {
      id: 2,
      name: "Jane",
      email: "jane@example.com",
      ticketCount: 1
    }
  ]
};

module.exports = concert;