const connection = require('./db')

function insertReservation(idConcert, idUtilisateur, ticketCount, dateReservation, remainingTickets, res) {
  // Insérer la réservation dans la base de données
  connection.query('INSERT INTO Reservation (id_concert, id_utilisateur, statut, date_reservation) VALUES (?, ?, "a_confirme", ?)',
    [idConcert, idUtilisateur, dateReservation],
    (error, results) => {
      if (error) throw error

      const reservationId = results.insertId;

      // Mettre à jour le nombre de places restantes pour le concert
      connection.query('UPDATE Concert SET nb_places = ? WHERE id = ?', [remainingTickets, idConcert], (error, results) => {
        if (error) throw error

        // Créer l'objet de réservation avec les données insérées
        const reservation = {
          id_concert: idConcert,
          id_utilisateur: idUtilisateur,
          statut: 'a_confirme',
          date_reservation: dateReservation
        };

        const confirmationUrl = `/concerts/${idConcert}/reservations/${reservationId}/confirmation`;
        const cancellationUrl = `/concerts/${idConcert}/reservations/${reservationId}/annulation`;

        res.status(201).json(createHalResponse(reservation, confirmationUrl, cancellationUrl));
      });
    })
}


module.exports = {insertReservation}